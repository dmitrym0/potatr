json.array!(@videos) do |video|
  json.extract! video, :id, :name, :hash, :paused_at, :user_id
  json.url video_url(video, format: :json)
end
