class MakeVideosUtf8 < ActiveRecord::Migration
  def change
      execute "ALTER TABLE videos CONVERT TO CHARACTER SET utf8"
  end
end
