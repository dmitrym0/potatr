class Video < ActiveRecord::Base
  belongs_to :user
  resourcify

  acts_as_taggable

  validates :video_hash, presence: true
  validates :video_hash, uniqueness: { scope: :user, message: "User already has this video"}

  paginates_per 12
  max_paginates_per 50

  def paused_at 
    read_attribute(:paused_at) || 0
  end

  def duration
    read_attribute(:duration) || 0
  end

  # small: 120x90
  # large: 480x360
  def thumbnail_url
  	read_attribute(:thumbnail_url) || 'http://placehold.it/480x360'
  end

  def update_remote_metadata
  	PotatrWorker.perform_async(self.video_hash,1)
  end

  def progress
    if self.paused_at == 0 || self.paused_at == nil 
      return 0
    end
    self.paused_at / self.duration.to_f * 100
  end

  # super ugly. the default delimiter for acts-as-taggable-on is a comma, but it server it up as 
  # an array
  def tag_list
    concat = ""
    super.each { | tag |  concat.length == 0 ? concat = tag : concat = concat + ", #{tag}" }
    concat
  end
end
