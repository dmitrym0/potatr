class TagsController < ApplicationController
	def index
		popular_tags = ActsAsTaggableOn::Tag.most_used
		user_tags = Video.where(user_id: 1).tag_counts_on(:tags)
		@tags = {popular_tags: popular_tags, user_tags: user_tags}
	end
end
