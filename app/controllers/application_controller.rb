class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # whut
  protect_from_forgery 
  before_action :authenticate_user!
  check_authorization :unless => :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    puts "Access denied"
    respond_to do |format|
      format.json { render nothing: true, status: :forbidden }
      format.html { redirect_after_login_or_main_root }
    end
  end


  private

  def redirect_after_login_or_main_root
    store_location_for :user, request.path
    redirect_to user_signed_in? ? main_app.root_url : new_user_session_path, alert: exception.message
  end
end


