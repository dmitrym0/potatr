FROM phusion/passenger-full:0.9.18

# do sidekiq things

RUN mkdir /etc/service/sidekiq
RUN mkdir /etc/service/sidekiq/env
#ADD sidekiq.rails.env.sh /etc/service/sidekiq/env/RAILS_ENV
ADD sidekiq.sh /etc/service/sidekiq/run
ADD webapp.conf /etc/nginx/sites-enabled/webapp.conf


# RUN mkdir /etc/service/bundle-check-install
# ADD bundle-check-install.sh /etc/service/bundle-check-install/run


# redis
RUN rm -f /etc/service/redis/down

ENV APP_HOME /home/app

RUN ruby-switch --set ruby2.2

RUN rm -f /etc/service/nginx/down

RUN rm /etc/nginx/sites-enabled/default

ENV RAILS_ASSETS_PRECOMPILE=1

CMD ["/sbin/my_init"]


RUN su app -c 'mkdir /home/app/{bundle,bundle-cache}'


# Install bundle (assuming bundle packaged to vendor/cache)
COPY potatr-env.conf /etc/nginx/main.d/potatr-env.conf
COPY vendor/cache /home/app/bundle-cache/vendor/cache
COPY Gemfile /home/app/bundle-cache/Gemfile
COPY Gemfile.lock /home/app/bundle-cache/Gemfile.lock
RUN chown -R app /home/app/bundle-cache
RUN su app -c 'cd /home/app/bundle-cache && \
               bundle install \
                    --jobs=4 \
                    --path=/home/app/bundle \
                    --no-cache'

WORKDIR $APP_HOME

ADD . $APP_HOME

RUN cp -a /home/app/bundle-cache/.bundle /home/app/
RUN mkdir -p log tmp public
RUN chown --recursive app log tmp public


RUN su -c 'DATABASE_URL=sqlite3:///tmp/dummy.sqlite3  RAILS_ENV=production bundle exec rake assets:precompile'
