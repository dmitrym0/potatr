require 'rake'
namespace :potatr do
  desc "Seed basic video data"
  task :seed => :environment do
  	File.open("db/videolist") do |file|
  		file.each do |line|
  			hash = line.gsub(/\n/,'')
  			v = Video.new
  			v.user = User.find(1)
  			v.video_hash = hash
  			v.paused_at = 0
  			v.save
  			v.update_remote_metadata
  		end
  	end
  end

  desc "Refetch all metadata for videos"
  task :refetch_metadata  => :environment do
      Video.all.each do |video|
        video.update_remote_metadata
    end
  end
end