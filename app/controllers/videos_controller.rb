class VideosController < ApplicationController
  #skip_before_action :verify_authenticity_token, if: :json_request?
  skip_before_action :verify_authenticity_token
  load_and_authorize_resource :through => :current_user
  before_action :set_video, only: [:show, :edit, :update, :destroy]
  after_action :allow_iframe, only: [:new, :show]


  # GET /videos
  # GET /videos.json
  def index
    @videos = Video.where(user: current_user).order(updated_at: :desc)
    if (params[:tag].present?) 
      @videos = @videos.tagged_with(params[:tag])
    end
    @user_tags = Video.where(user: @current_user).tag_counts_on(:tags).order('name')
    @videos = @videos.page params[:page]
  end

  # GET /videos/1
  # GET /videos/1.json
  def show
  end

  # GET /videos/new
  def new
    @video = Video.new
  end

  # GET /videos/1/edit
  def edit
  end

  # POST /videos
  # POST /videos.json
  def create
    @video = Video.new(video_params)
    @video.user = current_user
    respond_to do |format|
      if @video.save
        @video.update_remote_metadata
        format.html { redirect_to @video, notice: 'Video was successfully created.' }
        format.json { render :show, status: :created, location: @video }
      else
        format.html { render :new }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /videos/1
  # PATCH/PUT /videos/1.json
  def update
    respond_to do |format|
      if @video.update(video_params)
        format.html { redirect_to @video, notice: 'Video was successfully updated.' }
        format.json { render :show, status: :ok, location: @video }
      else
        format.html { render :edit }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video.destroy
    respond_to do |format|
      format.html { redirect_to videos_url, notice: 'Video was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def json_request?
      request.format.json?
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_params
      params.require(:video).permit(:name, :video_hash, :paused_at, :tag_list)
    end


    def allow_iframe
      response.headers.except! 'X-Frame-Options'
    end

end
