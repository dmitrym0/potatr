class PotatrWorker
  include Sidekiq::Worker

  def perform(video_hash, count)
  	Yt.configuration.api_key = "AIzaSyBAsTqVjLjnIC-bcnLRRNuPNHIp4l14cug"
  	video = Video.find_by_video_hash(video_hash)
  	logger.info "Processing video #{video.video_hash}"
  	remote_video = Yt::Video.new id: video.video_hash
  	# don't trigger callbacks
  	video.update_columns(title: remote_video.title, thumbnail_url: remote_video.thumbnail_url(:high), duration: remote_video.duration);
  end

end