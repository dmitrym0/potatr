$( document ).ready( function() {

	var player;
	var activeVideo;
	var trackingInterval;

	var loadYTPlayer = function() {
		console.log("Load player..");
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	};

	createYTPlayerForVideo = function(hash, timeoffset, video_id) {
		vid = video_id;
		if (player !== undefined) {
			player.destroy();
		}
        player = new YT.Player('player', {
          height: '390',
          width: '640',
          videoId: hash,
          playerVars: {
          	'start': timeoffset
          },
          events: {
            // 'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
	};

	function startTracking() {
		trackingInterval = setInterval(function() {
			console.log("Player:", player.getCurrentTime());
			var timings = {};
			timings.paused_at = player.getCurrentTime();

			$.ajax({
				type: "PUT",
				contentType: "application/json; charset=utf-8",
				url: location.origin + '/videos/' + vid,
				data: JSON.stringify(timings),
				dataType: "json",
				success: function (msg) {
				  console.log('Posted updated..');
				},
				error: function (err){
				  console.log('Error posting update');
				}
			});

			// activeVideo.set("pausedat", Math.floor(player.getCurrentTime()));
			// activeVideo.save();
		}, 2000);
	};

	function stopTracking() {
		console.log("Stopped.");
		clearInterval(trackingInterval);
	};


	function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
     	startTracking();
		} else {
			stopTracking();
		}
	};

	// videoListView.render();
	loadYTPlayer();
	
});


