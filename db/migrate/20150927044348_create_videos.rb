class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :name
      t.string :hash
      t.integer :paused_at
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
