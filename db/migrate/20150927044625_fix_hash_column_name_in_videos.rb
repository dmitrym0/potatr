class FixHashColumnNameInVideos < ActiveRecord::Migration
  def change
  	 rename_column :videos, :hash, :video_hash
  end
end
