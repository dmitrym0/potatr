class ImportController < ApplicationController
  load_and_authorize_resource :video, :through => :current_user

	def set
		@results = []

		list = params[:video_list].split(/\r\n/)
		# binding.pry
		list.each do |video|
			video_split_up = video.split(":")
			v = Video.new
			v.user = current_user
			v.video_hash = video_split_up[0]
			v.tag_list = video_split_up[1] if video_split_up.length > 1 
			v.paused_at = video_split_up[2] if video_split_up.length > 2
			begin
				v.save!
				@results.push "Saved #{v.video_hash}"
			rescue => e
				@results.push "Failed to add #{v.video_hash}: #{e}"
			end
		end
	end

	def index
	end

	def export
		@videos = Video.accessible_by(current_ability)
	end

end
